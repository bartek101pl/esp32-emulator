# ESP32 emulator

## Do rozpoczęcia urzywania emulatora podszebujemy:
- Systemu linux wersja np. serwerowa lub zwykła z gui 
- Na windows'ie instalujemy VSCode oraz wtyczkę "Remote - SSH". (Można mieć to zainstalowane na linux'ie ale uważam że bez sensu i szkoda zasobów)
- Putty na windows'ie

## Rozpoczęcie pracy

- importujemy repozytorium : [esp32-emulator](https://gitlab.com/SimLE/simba/simulation/can-module-emulator/esp32-emulator)
- wchodzimy do repozytorium za pomocą terminala i wykonujemy komendy: 
```
$ sudo chmod 777 ./install.sh
$ ./install.sh
$ mkdir build
```

## Użytkowanie emulatora

Emulator możemy odpalić na dwa sposoby
### Poprzez terminal
- poprzez wybrany edytor kodu dodajemy nasz kod do pliku src/main.cpp
- wchodzimy do folderu build (terminal)
- wykonujemy komendy 
```
$ cmake ..       <- generuje pliki konfiguracyjne do kompilacjie (MakeFile) 
$ cmake emulator <- buduje emulator (kompilacja)
$ ./src/emulator <- uruchamia zbudowany emulator
```

# Obecnie wspierane funkcjonalności:
- Magistrala Can wraz z potwierdzeiem otrzymania wiadomości przez inne urządzenia
- Podstawowa obsługa GPIO bez możliwości zewnętrznego ustawiania czy odczytu
- Dodana Biblioteka 107-ArduinoUAVCAN
- Serial.print(), Serial.println()