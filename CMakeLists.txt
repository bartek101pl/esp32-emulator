cmake_minimum_required(VERSION 3.2)

if(CMAKE_BUILD_TYPE)
	message(STATUS "CMake build type: ${CMAKE_BUILD_TYPE}")
endif()

# list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

project(esp32-emulator)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CPPLINT_LINE_LENGTH 120)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
# enable_testing()

# include(Cppcheck)
# include(Cpplint)
# include(Gcovr)
# include(Testing)
include_directories("${CMAKE_SOURCE_DIR}")
add_subdirectory(emulator)
add_subdirectory(lib)
add_subdirectory(src)
add_subdirectory(src2)
