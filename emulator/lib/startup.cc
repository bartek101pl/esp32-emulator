#include <iostream>
#include <memory>
#include <thread>

#include "emulator/lib/arduino-emulator/base.h"
#include "emulator/lib/can-bus-emulator/server/can_bus_server.h"

auto t_start = std::chrono::high_resolution_clock::now();

unsigned long millis() {
  auto t_end = std::chrono::high_resolution_clock::now();
  double elapsed_time_ms =
      std::chrono::duration<double, std::milli>(t_end - t_start).count();
  return static_cast<unsigned long>(elapsed_time_ms);
}

void delay(int miliseconds) {
  std::this_thread::sleep_for(std::chrono::milliseconds{miliseconds});
}

int main(int argc, char const* argv[]) {

  can_bus::server::CanBusServer* server;
  std::unique_ptr<sdbus::IConnection> conection;
  try {
    conection = sdbus::createSystemBusConnection(
        pl::simle::simba::canbus_adaptor::INTERFACE_NAME);
    server =
        new can_bus::server::CanBusServer{*conection, "/pl/simle/simba/canbus",true};
    conection->enterEventLoopAsync();
  } catch (const sdbus::Error& err) {
  }



  setup();
  while (true) {
    loop();
    std::this_thread::sleep_for(std::chrono::milliseconds{5});
  }

  return 0;
}
