/**
 * @file serial.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define Serial port emulator for esp32 emulator
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef EMULATOR_LIB_ARDUINO_EMULATOR_SERIAL_H_
#define EMULATOR_LIB_ARDUINO_EMULATOR_SERIAL_H_

#include <string>

class ESerial {
 private:
  /* data */
 public:
  void begin(unsigned short bit_rate);
  void begin(int bit_rate);
  void print(const std::string& text);
  void println(const std::string& text);
  void print(const int& text);
  void println(const int& text);
  void print(const uint32_t& text);
  void println(const uint32_t& text);
  ESerial() = default;
  ~ESerial() = default;
};
#endif  // EMULATOR_LIB_ARDUINO_EMULATOR_SERIAL_H_