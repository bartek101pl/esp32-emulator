
#include "emulator/lib/arduino-emulator/gpio.h"

#include <stdint.h>

uint64_t gpio_status = 0;

void digitalWrite(int pin, int flag) {
  uint64_t temp = gpio_status & (0xFFFFFFFFFFFFFFFF ^ (1 << pin));
  gpio_status = (temp | (flag << pin));
}
int digitalRead(int pin) {
  uint64_t temp = (gpio_status & (1 << pin));
  return (temp > 0) ? HIGH : LOW;
}
void pinMode(int pin, int mode) {

}