#ifndef EMULATOR_LIB_ARDUINO_EMULATOR_GPIO_H_
#define EMULATOR_LIB_ARDUINO_EMULATOR_GPIO_H_

#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0
void digitalWrite(int pin, int flag);
int digitalRead(int pin);
void pinMode(int pin, int mode);
#endif  // EMULATOR_LIB_ARDUINO_EMULATOR_GPIO_H_