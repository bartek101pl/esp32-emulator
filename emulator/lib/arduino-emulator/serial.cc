/**
 * @file serial.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define Serial port emulator for esp32 emulator
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "emulator/lib/arduino-emulator/serial.h"

#include <iostream>
void ESerial::begin(int bit_rate) {}
void ESerial::begin(unsigned short bit_rate) {}
void ESerial::print(const std::string& text) { std::cout << text; }
void ESerial::println(const std::string& text) {
  std::cout << text << std::endl;
}
void ESerial::print(const int& text) { std::cout << text; }
void ESerial::println(const int& text) { std::cout << text << std::endl; }
void ESerial::print(const uint32_t& text) { std::cout << text; }
void ESerial::println(const uint32_t& text) { std::cout << text << std::endl; }