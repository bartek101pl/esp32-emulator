#!/bin/bash
 # @file install_yakut.sh
 # @author Mateusz Krajewski (matikrajek42@gmail.com)
 # @brief automate install yakut
 # @version 1.0
 # @date 2023-03-01
 # 
 # @copyright Copyright (c) 2023
#


current_dir=$(pwd)
echo $current_dir

sudo apt update
echo "install dependencies"
sudo apt install python3 python3-pip python3-dev -y
sudo apt install can-utils libsdl2-dev libasound2-dev libjack-dev unzip wget git -y
pip3 install yakut

echo 'export CYPHAL_PATH="$HOME/.cyphal"'>> ~/.bashrc
echo 'export PATH="$PATH:/home/$USER/.local/bin"'>> ~/.bashrc

source ~/.bashrc

echo "copy cyphal data to ~/.cyphal"
mkdir -p ~/.cyphal 
wget https://github.com/OpenCyphal/public_regulated_data_types/archive/refs/heads/master.zip -O dsdl.zip
unzip dsdl.zip -d ~/.cyphal
mv -f ~/.cyphal/public_regulated_data_types*/* ~/.cyphal
wget https://github.com/Zubax/zubax_dsdl/archive/refs/heads/master.zip -O dsdl.zip
unzip dsdl.zip -d ~/.cyphal
mv -f ~/.cyphal/zubax_dsdl*/* ~/.cyphal
cd ~/.cyphal
yakut compile https://github.com/OpenCyphal/public_regulated_data_types/archive/refs/heads/master.zip

cd $current_dir
echo "run yakut_monitor.sh"
sh yakut-monitor.sh