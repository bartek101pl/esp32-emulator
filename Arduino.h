#ifndef ARDUINO_H_
#define ARDUINO_H_
#define SIMULATION
#include <chrono>
#include <thread>

#include "emulator/lib/arduino-emulator/base.h"
#include "emulator/lib/arduino-emulator/gpio.h"
#include "emulator/lib/arduino-emulator/serial.h"

unsigned long millis();

void delay(int miliseconds);
static ESerial Serial{};
#endif  // ARDUINO_H_