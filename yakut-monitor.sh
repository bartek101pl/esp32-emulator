#!/bin/bash
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set up vcan0

export UAVCAN__CAN__IFACE='socketcan:vcan0'
export UAVCAN__CAN__MTU=8
export UAVCAN__CAN__BITRATE="1000000 1000000"
export UAVCAN__NODE__ID=3
# export UAVCAN__NODE__ID=$(yakut accommodate)  # Pick an unoccupied node-ID automatically for this shell session.
# echo "Auto-selected node-ID for this session: $UAVCAN__NODE__ID"
yakut monitor
