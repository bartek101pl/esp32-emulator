#include <Arduino.h>
#include <SPI.h>  //include potrzebne do działania samego modułu CAN

#include "../lib/configs/can_module_config.h"
#include "../lib/mcp_can/mcp_can.h"  //include potrzebne do działania samego modułu CAN Waże  żeby użwyać tej bublioteki co jest z projektem a nie pobranej z sieci
// ponieważ nie zadziala wam bo w tej jest dodane mapowanie pinów dla SPI

MCP_CAN CAN0(15);  // W argumencie konstruktora podajemy nr. pinu CS w naszym
                   // przypadku jest to pin nr. 15

void setup() {
  // Tutaj inicjalizujemy moduł i ustawiamy jego parametry. Jako 2. parametr
  // podajemy prędkość magistrali i musi być taka sama dla wszystkich modłów w
  // sieci
  //  Ważne jako 3 argument musi być podana częstotliwość 16MHz inaczej moduł
  //  nie zadziała w naszym przypadku ponieważ mamy zmieniony oscylator z 8MHz
  //  na 16MHz
  if (CAN0.begin(MCP_ANY, CAN_1000KBPS, MCP_16MHZ) != CAN_OK) return;
  // Ustawiamy trub pracy modułu na normalny czyli tryb w którym nadaje on ACK
  // do każdej wiadomości.
  CAN0.setMode(MCP_NORMAL);
  // Ustawiamy że pin nr. 17 jest wejściowym i jeżeli pojawi się na nim stan
  // niski to oznacza że pojawiła się nowa wiadomość na magistrali CAN
  pinMode(_EXTERNAL_CAN_1_INT, INPUT);

  // CAN0.sendMsgBuf(<ID ramki max 29 bitów>, <ustawiamy '1' jeżeli mamy id
  // większe niż 11 bitów>,<Rozmiar danych do wysłania 0-8 bajtow>, <Wskaźnik na
  // tablice lub komurkę danych>);
}

void loop() {
  if (!digitalRead(_EXTERNAL_CAN_1_INT)) {
    long unsigned int rxId;  // tutuaj zostanie zapisany numer id ramki
    unsigned char len = 0;   // tutaj zostanie zapisana długosć danych
    unsigned char rxBuf[8];  // tutja zostaną zapisane dane

    CAN0.readMsgBuf(&rxId, &len, rxBuf);
    Serial.print("ID: ");
    Serial.print(static_cast<uint32_t>(rxId));
    Serial.print(" Length: ");
    Serial.print(len);
    for (size_t i = 0; i < len; i++)
    {
      Serial.print(" ");
      Serial.print(static_cast<uint8_t>(rxBuf[i]));
    }
    Serial.println("");
    
    // teraz mamuy odczytaną ramkę danych can
  }
}
