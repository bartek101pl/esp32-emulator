#!/bin/bash
sudo apt update
echo "Installing cmake and c++ compiler"
sudo apt-get install cmake -y
sudo apt-get install build-essential -y
sudo apt-get install gdb -y
sudo apt-get install pkg-config -y
sudo apt-get install libsystemd-dev -y
sudo apt install python3-pip -y
echo "Cmake and c++ compiler were installed "
echo "Installing spdlog"
sudo apt-get install libspdlog-dev -y
echo "Spdlog was installed"
echo "Installing sdbus-cpp"
sudo apt install libsdbus-c++-dev libsdbus-c++-bin -y
git clone https://github.com/Kistler-Group/sdbus-cpp.git
cd sdbus-cpp
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
sudo cmake --build . --target install
cd ../..
rm sdbus-cpp -rf
echo "Sdbus-cpp was installed "
echo "Adding sdbus configs"
sudo cp emulator/dbus-configs/* /etc/dbus-1/system.d/
ls -la /etc/dbus-1/system.d/
echo "Sdbus configs added"