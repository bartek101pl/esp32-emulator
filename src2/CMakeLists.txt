cmake_minimum_required(VERSION 3.1)

project(can-module-2 CXX)

set(MAIN_SOURCE_FILE "${CMAKE_CURRENT_SOURCE_DIR}/../emulator/lib/startup.cc")
file(GLOB_RECURSE SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")
list(REMOVE_ITEM SOURCES "${MAIN_SOURCE_FILE}")

find_package(spdlog CONFIG REQUIRED)

add_library(${PROJECT_NAME}-impl INTERFACE)

target_sources(${PROJECT_NAME}-impl
	INTERFACE ${SOURCES}
)

add_executable(${PROJECT_NAME})

target_sources(${PROJECT_NAME}
	PRIVATE "${MAIN_SOURCE_FILE}"
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE ${PROJECT_NAME}-impl
    spdlog::spdlog
    can-bus-server
    can-bus-client
    can-bus-components
    arduino-emulator-base
    esp-lib
)


# add_cppcheck(${PROJECT_NAME})
# add_cpplint(${PROJECT_NAME})