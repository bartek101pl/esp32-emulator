#include <Arduino.h>
#include <SPI.h>
#include <lib/107-Arduino-Cyphal/src/ArduinoUAVCAN.h>
#include "../lib/configs/can_module_config.h"
#include "lib/mcp_can/mcp_can.h"

using std::isfinite;

bool transmitCanFrame(CanardFrame const &);
void onGetInfo_1_0_Request_Received(CanardTransfer const &, ArduinoUAVCAN &);
static const uint8_t deviceID = 9;
uavcan_node_GetInfo_Response_1_0 GET_INFO_DATA = {
    /// uavcan.node.Version.1.0 protocol_version
    {1, 0},
    /// uavcan.node.Version.1.0 hardware_version
    {1, 3},
    /// uavcan.node.Version.1.0 software_version
    {1, 3},
    /// saturated uint64 software_vcs_revision_id
    NULL,
    /// saturated uint8[16] unique_id in last set device id
    {0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xf0, 0xf0,
     0xf0, 0xff, 0xff, deviceID},
    /// saturated uint8[<=50] name
    {"pl.simle.r6.srad", strlen("pl.simle.r6.srad")},
};

// CAN RX Variables
long unsigned int rxId;
unsigned char len;
unsigned char rxBuf[8];

// Serial Output String Buffer
char msgString[128];

// CAN0 INT and CS
#define CAN0_INT 17  // Set INT to pin 2
MCP_CAN CAN0(15);    // Set CS to pin 10

ArduinoUAVCAN uc(deviceID, transmitCanFrame);
uavcan::node::Heartbeat_1_0<> hb;

void setup() {
  Serial.begin(115200);
  // Initialize MCP2515 running at 16MHz with a baudrate of 500kb/s and the
  // masks and filters disabled.
  if (CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_16MHZ) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");
  CAN0.setMode(MCP_NORMAL);

  pinMode(CAN0_INT, INPUT);  // Configuring pin for /INT input

  hb.data.uptime = 0;
  hb = uavcan::node::Heartbeat_1_0<>::Health::NOMINAL;
  hb = uavcan::node::Heartbeat_1_0<>::Mode::INITIALIZATION;
  hb.data.vendor_specific_status_code = 0;
  uc.subscribe<uavcan::node::GetInfo_1_0::Request<>>(
      onGetInfo_1_0_Request_Received);
}
void onReceiveCanFrame() {
  long unsigned int rxId;
  unsigned char len = 0;
  unsigned char rxBuf[8];

  CAN0.readMsgBuf(&rxId, &len, rxBuf);

  CanardFrame const f{
      1,                               /* timestamp_usec  */
      (static_cast<uint32_t>(rxId) & 0x1FFFFFFF),             /* extended_can_id limited to 29 bit */
      static_cast<uint8_t const>(len), /* payload_size    */
      reinterpret_cast<const void *>(rxBuf) /* payload         */
  };
  uc.onCanFrameReceived(f);
 }

void loop() {
  hb.data.uptime = millis() / 1000;
  hb = uavcan::node::Heartbeat_1_0<>::Mode::OPERATIONAL;

  static unsigned long prev = 0;
  unsigned long const now = millis();
  if (now - prev > 1000) {
    uc.publish(hb);
    prev = now;
  }
  if (!digitalRead(_EXTERNAL_CAN_1_INT)) {
    onReceiveCanFrame();
  }

  while (uc.transmitCanFrame()) {
  }
}

bool transmitCanFrame(CanardFrame const &frame) {
  uint8_t data[8];
  memcpy(data, frame.payload, frame.payload_size);
  return CAN0.sendMsgBuf(frame.extended_can_id, 1, frame.payload_size, data) ==
         CAN_OK;
}

void onGetInfo_1_0_Request_Received(CanardTransfer const &transfer,
                                    ArduinoUAVCAN &uc) {
  uavcan::node::GetInfo_1_0::Response<> rsp =
      uavcan::node::GetInfo_1_0::Response<>();
  rsp.data = GET_INFO_DATA;
  uc.respond(rsp, transfer.remote_node_id, transfer.transfer_id);
}
