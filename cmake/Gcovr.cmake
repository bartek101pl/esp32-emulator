cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

add_custom_target(gcovr)

find_program(FIND_PATH find)
find_program(GCOVR_PATH gcovr)

if(GCOVR_PATH)
	execute_process(COMMAND "${GCOVR_PATH}" "--version"
		OUTPUT_VARIABLE GCOVR_VERSION
	)

	string(REGEX MATCH "[0-9]+\\.[0-9]+" GCOVR_VERSION "${GCOVR_VERSION}")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(find
	REQUIRED_VARS FIND_PATH
)
find_package_handle_standard_args(gcovr
	REQUIRED_VARS GCOVR_PATH
	VERSION_VAR GCOVR_VERSION
)

function(add_gcovr TARGET)
	if(NOT GCOVR_FOUND)
		return()
	endif()

	set(GCOVR_TARGET "${TARGET}-gcovr")
	set(GCOVR_OUTPUT "${CMAKE_BINARY_DIR}/${GCOVR_TARGET}")
	set(GCOVR_OUTPUT_HTML "${GCOVR_OUTPUT}/report.html")

	get_target_property(SOURCE_DIR ${TARGET} SOURCE_DIR)

	add_custom_target(${GCOVR_TARGET}
		COMMAND "${FIND_PATH}" "${CMAKE_CURRENT_BINARY_DIR}" -name "*.gcda" -type f -delete
		COMMENT "Deleting .gcda files"
	)

	add_custom_command(
		TARGET ${GCOVR_TARGET} PRE_BUILD
		COMMAND "${CMAKE_CTEST_COMMAND}"
		COMMENT "Running tests"
	)

	add_custom_command(
		TARGET ${GCOVR_TARGET} POST_BUILD
		COMMAND "${CMAKE_COMMAND}" -E make_directory "${GCOVR_OUTPUT}"
		COMMAND "${CMAKE_COMMAND}" -E rm -rf "${GCOVR_OUTPUT}/*"
		COMMAND "${GCOVR_PATH}"
			--root "${SOURCE_DIR}"
			--txt
			--html-details "${GCOVR_OUTPUT_HTML}"
			--print-summary
			--gcov-executable "$ENV{CROSS_COMPILE}gcov"
			--object-directory "${CMAKE_CURRENT_BINARY_DIR}"
		COMMENT "Generating gcovr coverage report for ${TARGET}"
	)

	add_dependencies(${GCOVR_TARGET} ${ARGN})

	add_dependencies(gcovr ${GCOVR_TARGET})
endfunction(add_gcovr)
