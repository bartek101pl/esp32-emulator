cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

add_custom_target(cppcheck)

find_program(CPPCHECK_PATH cppcheck)

if(CPPCHECK_PATH)
	execute_process(COMMAND "${CPPCHECK_PATH}" "--version"
		OUTPUT_VARIABLE CPPCHECK_VERSION
	)

	string(REGEX MATCH "[0-9]+\\.[0-9]+" CPPCHECK_VERSION "${CPPCHECK_VERSION}")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(cppcheck
	REQUIRED_VARS CPPCHECK_PATH
	VERSION_VAR CPPCHECK_VERSION
)

function(add_cppcheck TARGET)
	if(NOT CPPCHECK_FOUND)
		return()
	endif()

	get_target_property(SOURCE_DIR ${TARGET} SOURCE_DIR)

	add_custom_target(${TARGET}-cppcheck
		COMMAND "${CPPCHECK_PATH}"
			--enable=all
			--inconclusive
			--inline-suppr
			--language=c++
			--platform=unix32
			--relative-paths=${CMAKE_CURRENT_SOURCE_DIR}
			--suppress=missingIncludeSystem
			-I ${CMAKE_SOURCE_DIR}
			${SOURCE_DIR}
		COMMENT "Executing cppcheck for ${TARGET}"
	)

	add_dependencies(cppcheck ${TARGET}-cppcheck)
endfunction(add_cppcheck)

