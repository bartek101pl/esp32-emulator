cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

include(Qemu)

function(add_ctest TARGET)
	if(CMAKE_CROSSCOMPILING)
		get_qemu_command(${TARGET} TEST_COMMAND)

		if(NOT TEST_COMMAND)
			message(WARNING "Unable to get QEMU command for running ${TARGET}")
			return()
		endif()
	else()
		set(TEST_COMMAND ${TARGET})
	endif()

	set(TEST_BUILD ${TARGET}-build)
	set(TEST_FIXTURE ${TARGET}-fixture)

	add_test(
		NAME ${TEST_BUILD}
		COMMAND "${CMAKE_COMMAND}"
			--build "${CMAKE_BINARY_DIR}"
			--config $<CONFIG>
			--target ${TARGET}
	)

	set_tests_properties(${TEST_BUILD} PROPERTIES
		FIXTURES_SETUP ${TEST_FIXTURE}
		RESOURCE_LOCK cmake-build
	)

	add_test(
		NAME ${TARGET}
		COMMAND ${TEST_COMMAND}
	)

	set_tests_properties(${TARGET} PROPERTIES
		FIXTURES_REQUIRED ${TEST_FIXTURE}
	)
endfunction(add_ctest)
