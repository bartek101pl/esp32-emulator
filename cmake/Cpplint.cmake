cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

add_custom_target(cpplint)

find_program(CPPLINT_PATH cpplint.py
	PATH "${CMAKE_CURRENT_LIST_DIR}"
)

if(CPPLINT_PATH)
	execute_process(COMMAND "${CPPLINT_PATH}" "--version"
		OUTPUT_VARIABLE CPPLINT_VERSION
	)

	string(REGEX MATCH "[0-9]+\\.[0-9]+\\.[0-9]+" CPPLINT_VERSION "${CPPLINT_VERSION}")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(cpplint
	REQUIRED_VARS CPPLINT_PATH
	VERSION_VAR CPPLINT_VERSION
)

function(add_cpplint TARGET)
	if(NOT CPPLINT_FOUND)
		return()
	endif()
	
	get_target_property(SOURCE_DIR ${TARGET} SOURCE_DIR)

	if(CPPLINT_LINE_LENGTH GREATER 0)
		list(APPEND CPPLINT_ARGS "--linelength=${CPPLINT_LINE_LENGTH}")
	endif()

	add_custom_target(${TARGET}-cpplint
		COMMAND "${CPPLINT_PATH}"
			--recursive
			${CPPLINT_ARGS}
			${SOURCE_DIR}
		COMMENT "Executing cpplint for ${TARGET}"
	)

	add_dependencies(cpplint ${TARGET}-cpplint)
endfunction(add_cpplint)
