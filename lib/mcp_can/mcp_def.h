#ifndef MCP_DEF_H_H
#define MCP_DEF_H_
#define MCP_STDEXT 0 /* Standard and Extended        */
#define MCP_STD 1    /* Standard IDs ONLY            */
#define MCP_EXT 2    /* Extended IDs ONLY            */
#define MCP_ANY 3    /* Disables Masks and Filters   */

#define MCP_20MHZ 0
#define MCP_16MHZ 1
#define MCP_8MHZ 2
#define MCP_CLOCK_SELECT 3
#define MCP_CLKOUT_ENABLE 4
#define MCP_NORMAL 0x00
#define MCP_SLEEP 0x20
#define MCP_LOOPBACK 0x40
#define MCP_LISTENONLY 0x60
#define MODE_CONFIG 0x80
#define MODE_POWERUP 0xE0
#define MODE_MASK 0xE0
#define ABORT_TX 0x10
#define MODE_ONESHOT 0x08
#define CLKOUT_ENABLE 0x04
#define CLKOUT_DISABLE 0x00
#define CLKOUT_PS1 0x00
#define CLKOUT_PS2 0x01
#define CLKOUT_PS4 0x02
#define CLKOUT_PS8 0x03

#define CAN_4K096BPS 0
#define CAN_5KBPS 1
#define CAN_10KBPS 2
#define CAN_20KBPS 3
#define CAN_31K25BPS 4
#define CAN_33K3BPS 5
#define CAN_40KBPS 6
#define CAN_50KBPS 7
#define CAN_80KBPS 8
#define CAN_100KBPS 9
#define CAN_125KBPS 10
#define CAN_200KBPS 11
#define CAN_250KBPS 12
#define CAN_500KBPS 13
#define CAN_1000KBPS 14

#define CAN_OK (0)
#define CAN_FAILINIT (1)
#define CAN_FAILTX (2)
#define CAN_MSGAVAIL (3)
#define CAN_NOMSG (4)
#define CAN_CTRLERROR (5)
#define CAN_GETTXBFTIMEOUT (6)
#define CAN_SENDMSGTIMEOUT (7)
#define CAN_FAIL (0xff)
#endif