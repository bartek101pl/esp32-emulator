
#include "lib/mcp_can/mcp_can.h"

#include <vector>

#include "emulator/lib/arduino-emulator/gpio.h"
#include "lib/configs/can_module_config.h"

MCP_CAN::MCP_CAN(unsigned char _CS) {
  if (_CS == _CS_CAN_1) {
    this->client = new can_bus::client::CanBusClient(
        pl::simle::simba::canbus_proxy::INTERFACE_NAME,
        "/pl/simle/simba/canbus",
        []() { digitalWrite(_EXTERNAL_CAN_1_INT, LOW); });
    this->client->SetInterfaceId(0);
    digitalWrite(_EXTERNAL_CAN_1_INT, HIGH);
  } else if (_CS == _CS_CAN_2) {
    this->client = new can_bus::client::CanBusClient(
        pl::simle::simba::canbus_proxy::INTERFACE_NAME,
        "/pl/simle/simba/canbus",
        []() { digitalWrite(_EXTERNAL_CAN_2_INT, LOW); });
    this->client->SetInterfaceId(1);
    digitalWrite(_EXTERNAL_CAN_2_INT, HIGH);
  }
}
MCP_CAN::~MCP_CAN() { delete this->client; }
unsigned char MCP_CAN ::begin(unsigned char idmodeset, unsigned char speedset,
                              unsigned char clockset) {
  return 0;
}
unsigned char MCP_CAN ::init_Mask(unsigned char num, unsigned char ext,
                                  uint32_t ulData) {
  return 0;
}
unsigned char MCP_CAN ::init_Mask(unsigned char num, uint32_t ulData) {
  return 0;
}
unsigned char MCP_CAN ::init_Filt(unsigned char num, uint32_t ulData) {
  return 0;
}
void MCP_CAN ::setSleepWakeup(unsigned char enable) {}

unsigned char MCP_CAN ::setMode(unsigned char opMode) { return 0; }
unsigned char MCP_CAN ::sendMsgBuf(long unsigned int id, unsigned char ext,
                                   unsigned char len, unsigned char *buf) {
  std::vector<uint8_t> buff;
  for (size_t i = 0; i < len; i++) {
    buff.push_back(static_cast<uint8_t>(buf[i]));
  }
  can_bus::components::CanFrame fr{static_cast<uint32_t>(id), buff, 0, 0};
  bool status = this->client->TransferFrame(fr);
  if (status) {
    return CAN_OK;
  } else {
    return CAN_FAILTX;
  }
}
unsigned char MCP_CAN ::sendMsgBuf(long unsigned int id, unsigned char len,
                                   unsigned char *buf) {
  std::vector<uint8_t> buff;
  for (size_t i = 0; i < len; i++) {
    buff.push_back(static_cast<uint8_t>(buf[i]));
  }
  can_bus::components::CanFrame fr{static_cast<uint32_t>(id), buff, 0, 0};
  bool status = this->client->TransferFrame(fr);
  if (status) {
    return CAN_OK;
  } else {
    return CAN_FAILTX;
  }
}
unsigned char MCP_CAN ::readMsgBuf(long unsigned int *id, unsigned char *ext,
                                   unsigned char *len, unsigned char *buf) {
  if (this->client->FrameAvailable()) {
    can_bus::components::CanFrame fr = this->client->GetFrame();
    *id = static_cast<long unsigned int>(fr.GetId());
    *len = static_cast<unsigned char>(fr.GetPayload().size());
    *ext = 1;
    for (size_t i = 0; i < *len; i++) {
      buf[i] = static_cast<unsigned char>(fr.GetPayload()[i]);
    }
    if (!this->client->FrameAvailable()) {
      digitalWrite(17, HIGH);
    }
    return CAN_OK;
  }
  return CAN_FAIL;
}
unsigned char MCP_CAN ::readMsgBuf(long unsigned int *id, unsigned char *len,
                                   unsigned char *buf) {
  if (this->client->FrameAvailable()) {
    can_bus::components::CanFrame fr = this->client->GetFrame();
    *id = static_cast<long unsigned int>(fr.GetId());
    *len = static_cast<unsigned char>(fr.GetPayload().size());
    for (size_t i = 0; i < *len; i++) {
      buf[i] = static_cast<unsigned char>(fr.GetPayload()[i]);
    }
    if (!this->client->FrameAvailable()) {
      digitalWrite(17, HIGH);
    }
    return CAN_OK;
  }
  return CAN_FAIL;
}
unsigned char MCP_CAN ::checkReceive(void) { return 0; }
unsigned char MCP_CAN ::checkError(void) { return 0; }
unsigned char MCP_CAN ::getError(void) { return 0; }
unsigned char MCP_CAN ::errorCountRX(void) { return 0; }
unsigned char MCP_CAN ::errorCountTX(void) { return 0; }
unsigned char MCP_CAN ::enOneShotTX(void) { return 0; }
unsigned char MCP_CAN ::disOneShotTX(void) { return 0; }
unsigned char MCP_CAN ::abortTX(void) { return 0; }
unsigned char MCP_CAN ::setGPO(unsigned char data) { return 0; }
unsigned char MCP_CAN ::getGPI(void) { return 0; }
