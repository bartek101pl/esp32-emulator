#ifndef LIB_MCP_CAN_MCP_CAN_H_
#define LIB_MCP_CAN_MCP_CAN_H_

#include "lib/mcp_can/mcp_def.h"
#include "emulator/lib/can-bus-emulator/client/can_bus_client.h"
#include "emulator/lib/can-bus-emulator/components/can_frame.h"
#include <stdint.h>

class MCP_CAN {
 private:
  can_bus::client::CanBusClient* client;

      unsigned char m_nExtFlg;  // Identifier Type
                          // Extended (29 bit) or Standard (11 bit)
  uint32_t m_nID;     // CAN ID
  unsigned char m_nDlc;     // Data Length Code
  unsigned char m_nDta[8];  // Data array
  unsigned char m_nRtr;     // Remote request flag
  unsigned char m_nfilhit;  // The number of the filter that matched the message
  unsigned char MCPCS;      // Chip Select pin number
  unsigned char mcpMode;    // Mode to return to after configurations are performed.

  /*********************************************************************************************************
   *  mcp2515 driver function
   *********************************************************************************************************/
  // private:
 private:
 public:
  MCP_CAN(unsigned char _CS);
  ~MCP_CAN();
  unsigned char begin(unsigned char idmodeset, unsigned char speedset,
                unsigned char clockset);  // Initialize controller parameters
  unsigned char init_Mask(unsigned char num, unsigned char ext,
                    uint32_t ulData);               // Initialize Mask(s)
  unsigned char init_Mask(unsigned char num, uint32_t ulData);  // Initialize Mask(s)
  unsigned char init_Filt(unsigned char num, unsigned char ext,
                    uint32_t ulData);               // Initialize Filter(s)
  unsigned char init_Filt(unsigned char num, uint32_t ulData);  // Initialize Filter(s)
  void setSleepWakeup(
      unsigned char enable);  // Enable or disable the wake up interrupt (If disabled
                        // the MCP2515 will not be woken up by CAN bus activity)
  unsigned char setMode(unsigned char opMode);  // Set operational mode
  unsigned char sendMsgBuf(long unsigned int id, unsigned char ext, unsigned char len,
                     unsigned char *buf);  // Send message to transmit buffer
  unsigned char sendMsgBuf(long unsigned int id, unsigned char len,
                     unsigned char *buf);  // Send message to transmit buffer
  unsigned char readMsgBuf(long unsigned int *id, unsigned char *ext, unsigned char *len,
                     unsigned char *buf);  // Read message from receive buffer
  unsigned char readMsgBuf(long unsigned int *id, unsigned char *len,
                     unsigned char *buf);  // Read message from receive buffer
  unsigned char checkReceive(void);        // Check for received data
  unsigned char checkError(void);          // Check for errors
  unsigned char getError(void);            // Check for errors
  unsigned char errorCountRX(void);        // Get error count
  unsigned char errorCountTX(void);        // Get error count
  unsigned char enOneShotTX(void);         // Enable one-shot transmission
  unsigned char disOneShotTX(void);        // Disable one-shot transmission
  unsigned char abortTX(void);             // Abort queued transmission(s)
  unsigned char setGPO(unsigned char data);      // Sets GPO
  unsigned char getGPI(void);              // Reads GPI
};

#endif  // LIB_MCP_CAN_MCP_CAN_H_